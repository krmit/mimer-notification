import fs from "fs-extra";
import path from "path";
import yaml from "js-yaml";
import colors from "colors";

const program_dir = __dirname;
const data_path = `${program_dir}/data.yaml`;

fs.readFile(data_path, { encoding: "utf-8" })
    .then(yaml.safeLoad)
    .then(data => {
		console.log(data.next);
		data.next = data.next+1;
		fs.writeFile(data_path, yaml.safeDump(data));
    });

